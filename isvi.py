from flask import Flask
from prometheus_client import Counter, generate_latest, CONTENT_TYPE_LATEST
from flask import Response

app = Flask(__name__)

# Définir une métrique personnalisée avec Prometheus Python Client
custom_metric = Counter('my_custom_metric', 'Description of custom metric')

@app.route('/metrics')
def metrics():
    return Response(generate_latest(), mimetype=CONTENT_TYPE_LATEST)

@app.route('/')
def index():
    # Incrémenter la métrique personnalisée à chaque appel à l'index
    custom_metric.inc()
    return 'Hello, World!'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
