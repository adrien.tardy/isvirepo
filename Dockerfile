# Utiliser une image de base Python
FROM python:3.9-slim

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le code source dans le conteneur
COPY isvi.py /app/isvi.py

# Installer les dépendances Python
RUN pip install flask prometheus_client

# Exposer le port 5000 pour Flask
EXPOSE 5000

# Commande pour exécuter l'application Flask
CMD ["python", "isvi.py"]
