# Lab05 - Intro à Docker


## P1 Décrire les principales étape que docker a réalisé pour vous livrer le résultat de l’exécution de l’application à la console

D'abord il essaie de trouver l'image en local s'il ne la trouve pas il va la télécharger sur dockerHub puis l'éexecute.

## P2 Que constatez-vous ?

Le container est immédiatement supprimé après que l'execution est terminée


## P3 Que constatez-vous ?

On a lancé un container et avec "-it" (interactif), on se retrouve dans le conteneur ubuntu  
![alt text](image.png)

## P4 Que constatez-vous ? En vous référant aux “Cheatsheets” des annexes, déterminez et documentez la différence qu’il y a entre les commandes docker run et docker exec.

Docker run permet sert à créer un nouveau container alors qu'avec exec, le container est déjà en train de tourner et on oouvre un shell dans ce container.

## P5 Décrivez en détail le contenu de ce fichier. Aidez-vous en utilisant la Dockerfile Cheasheet.
```docker
FROM python:latest
COPY index.html /app/
WORKDIR /app
EXPOSE 8080
ENTRYPOINT ["python3", "-m", "http.server", "8080"]
```
FROM : From which image will this image will be based on.  
COPY : Copie des nouveaux fichiers ou dossiers and les ajoutes dans le destination.  
WORKDIR : Set le working directory pour les commandes qui suivent : RUN, CMD, ENTRYPOINT, COPY, and ADD.  
EXPOSE : Inform docker que le container écoutera sur le port spécifié. Mais attention ne rend pas le ports accessible par l'host.  
ENTRYPOINT : Permet de configurer le container pour run un executable. Allows you to configure a container that will run as an executable.
Command line arguments to docker run <image> will be appended after all elements in an exec form ENTRYPOINT and will override all elements specified using CMD.
The shell form prevents any CMD or run command line arguments from being used, but the ENTRYPOINT will start via the shell. This means the executable will not be PID 1 nor will it receive UNIX signals. Prepend exec to get around this drawback.
Only the last ENTRYPOINT instruction in the Dockerfile will have an effect.

Décrivez chacun des paramètres de cette commande. Comment faudrait-il la modifier pour exposer votre service sur un autre port ? Validez le fonctionnement du container nouvellement exposé.  
```
docker run --rm --name my-hello-heia --detach --publish 8080:8080 hello-heia
```
--rm : va suppimer le container une fois qu'il sera stopper.  
--name : défini un nom pour le conatainer.  
--detach : Lance le container en daemon.  
--publish : Il s'agit du "Pat".

## P7 

Pour push le commit et le tage ne même temps avec une initialisation :
```
git push --atomic origin <branch name> <tag>
```

Ensuite la CI/CD va runner:
```yaml
# This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml

# Build a Docker image with CI/CD and push to the GitLab registry.
# Docker-in-Docker documentation: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
#
# This template uses one generic job with conditional builds
# for the default branch and all other (MR) branches.

docker-build:
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - docker login -u "$dockerUser" -p "$dockerPass"
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  # All branches are tagged with $DOCKER_IMAGE_NAME (defaults to commit ref slug)
  # Default branch is also tagged with `latest`
  script:
    - docker build -t "$DOCKER_IMAGE_NAME" .
    - docker push "$DOCKER_IMAGE_NAME"
  rules: 
    - if: $CI_COMMIT_TAG
    - changes:
      - .gitlab-ci.yml
```
 Puis en local pour download l'image du repo :
 En premier se loguer au repository gitlab
 ```
docker login registry.forge.hefr.ch/embsys/2022-2023/classe-jacques-supcik/group-4/isvitpdocker
 ```
Ensuite pour lancer le container avec le tag en question  [v0.3] -> [v0-3]
```
 docker run --name my-hello-heiaTag --detach --publish 8082:8082 registry.forge.hefr.ch/embsys/2022-2023/classe-jacques-supcik/group-4/isvitpdocker:v0-3
```

